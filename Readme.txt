/* 
Project Name : Alien Registration Programme
Project language : JAVA
Proect Author Name : Shubham Mittal
Project created on : Sun, 28th September 2014
Project Description : To register aliens on earth and generate PDF/ plaintext document according to
user's needs.
*/

Driver package : com.registration.driver
Driver Class : RegistrationMenu.java (main class)

 How to run the console application:
1. Compile the main class using Java compiler.
2. Follow the registration steps.
3. File will be generated in the source folder. 

How to extend the code to add new format.
1. Increment the "noOfFileTypes" variable in the driver class.(Currently initialised as 2 for pdf and text format)
2. Add the class of the required format type in com.registration.printer package. 
3. Open the class printDetails in the package com.registration.printer and add your functioncall of the previously created class to print the details in the new format.
	3.1 Refer the following code snippet:
			/***************************/
				driver program
				
				1. PDF
				2. txt
				3. xyz(added by you)
			/***************************/
 
		else if ( type == 3 ) {
			xyzGenerator xyzGenerator = new xyzGenerator();
			this.setMessage(xyzGenerator.detailsReaderxyz(key, registrationdatabase));

		}
	
4. For function calling please refer to the syntax functionname(object registrationdata, string registration key, int type)


Limitations and Scope:
Database has not been included.
With inclusion of database of your choice(can be done using jpa, or jdbc, etc), the above project can serve as a registration server with little modification in the implemented datastructure.



Refer to the snapshots for more information or kindly write to me at shubham9910103545@gmail.com