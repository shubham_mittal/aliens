package com.registration.data;

public class RegistrationInfo {
	
	private String codeName;
	private String bloodColor;
	private Integer legsCount;
	private Integer antennasCount;
	private String homePlanet;
	private String durationOfStay;
	
	

	public String getCodeName() {
		return codeName;
	}



	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}



	public String getBloodColor() {
		return bloodColor;
	}



	public void setBloodColor(String bloodColor) {
		this.bloodColor = bloodColor;
	}



	public Integer getLegsCount() {
		return legsCount;
	}



	public void setLegsCount(Integer legsCount) {
		this.legsCount = legsCount;
	}



	public Integer getAntennasCount() {
		return antennasCount;
	}



	public void setAntennasCount(Integer antennasCount) {
		this.antennasCount = antennasCount;
	}



	public String getHomePlanet() {
		return homePlanet;
	}



	public void setHomePlanet(String homePlanet) {
		this.homePlanet = homePlanet;
	}



	public String getDurationOfStay() {
		return durationOfStay;
	}



	public void setDurationOfStay(String durationOfStay) {
		this.durationOfStay = durationOfStay;
	}

	



}
